<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* factory(App\Contacto::class,10)->create(); */
        factory(App\User::class, 50)->create();
    }
}
