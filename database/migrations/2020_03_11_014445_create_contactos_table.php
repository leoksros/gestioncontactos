<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('nombre');
            $table->string('estado')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->date('fecha_contacto')->nullable();
            $table->date('ultimo_contacto')->nullable();
            $table->date('inicio_aseguradora')->nullable();
            $table->string('cuit')->nullable();

           

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
}
