<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contacto;
use Faker\Generator as Faker;
Use App\Art;
Use App\Categoria;

$factory->define(Contacto::class, function (Faker $faker) {
    return [

        'nombre' => $faker->name,
        'estado' => $faker->sentence(3),
        'id_art' => Art::inRandomOrder()->value('id') ?: factory(Art::class),
        'inicio_aseguradora' => $faker->date,
        'email' => $faker->unique()->safeEmail,
        'telefono' => $faker->randomDigit(),
        'fecha_contacto' => $faker->date,
        'ultimo_contacto' => $faker->date,
        'id_categoria' => Categoria::inRandomOrder()->value('id') ?: factory(Categoria::class),
        
    ];
});
