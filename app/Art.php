<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Art extends Model
{
    public $table = "arts";
    public $guarded = [];
    
    public function contactos()
    {
        return $this->hasMany('App\Contacto');
    }

}
