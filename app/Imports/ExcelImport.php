<?php

namespace App\Imports;
use App\Contacto;
use Carbon\Carbon;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExcelImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {   
        
     
        foreach($collection as $item)
        {
            
            $contacto = new Contacto;

            

            $contacto->nombre = $item[0];
            $contacto->estado = $item[1];
            $contacto->email = $item[4];
            $contacto->telefono = $item[5];
            $contacto->fecha_contacto = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($item[3]))->format('Y/n/j');
            $contacto->ultimo_contacto = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($item[6]))->format('Y/n/j');
            $contacto->inicio_aseguradora = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($item[7]))->format('Y/n/j');
            $contacto->categoria_id = 1;
            $contacto->user_id = 1;
            $contacto->art_id = $item[2];
            $contacto->cuit = $item[8];

            $contacto->save();
            
        }

       
        
    }

    
}
