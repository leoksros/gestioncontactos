<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use App\Imports\ExcelImport;
Use App\Contacto;
Use App\Art;
use App\Categoria;

class ContactosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactos = Contacto::orderBy('ultimo_contacto', 'desc')->paginate(20);
        
        return view('contactos.contactos',compact('contactos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arts = Art::all();
        $categorias = Categoria::all();
        return view('contactos.agregar',compact('arts','categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $nuevoContacto = new Contacto();
        $nuevoContacto->nombre = $request["name"];
        $nuevoContacto->estado = $request["estado"];
        $nuevoContacto->art_id = $request["art_id"];
        $nuevoContacto->inicio_aseguradora = $request["inicio_aseguradora"];
        $nuevoContacto->email = $request["email"];
        $nuevoContacto->telefono = $request["telefono"];
        $nuevoContacto->fecha_contacto = $request["fecha_contacto"];
        $nuevoContacto->ultimo_contacto = $request["ultimo_contacto"];
        $nuevoContacto->categoria_id = $request["categoria_id"];
        $nuevoContacto->user_id = Auth::id();
       
        $nuevoContacto->save();

        return redirect('contactos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $contacto = Contacto::find($id);
        $arts = Art::all();               
        return view('contactos.modificar',compact('contacto','arts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contacto $contacto)
    {
        
        $contacto->update([

            'nombre' => $request["nombre"],
            'estado' => $request["estado"],
            'art_id' => $request["art_id"],
            'inicio_aseguradora' => $request["inicio_aseguradora"],
            'email' => $request["email"],
            'telefono' => $request["telefono"],
            'fecha_contacto' => $request["fecha_contacto"],
            'ultimo_contacto' => $request["ultimo_contacto"],        

        ]);

        return redirect()->route('contactos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contacto $contacto)
    {

        $contacto->delete();
        return redirect()->route('contactos')->with('status','Contacto eliminado.');

    }

    public function import(Request $request)
    {
        $file = $request->file;
        
        Excel::import(new ExcelImport, $file);
        
        $contactos = Contacto::all()->load('art');


        return view('/');

    }

    public function search(Request $request)
    {
        $contactos = Contacto::where('nombre','LIKE',"%$request->busqueda%")->paginate(20);
        
        return view('contactos.contactos',compact('contactos'));
    }

    public function pendientes()
    {
        $contactos = Contacto::whereDate('fecha_contacto','=', Carbon::now()->toDateString())->paginate(20);
        return view('contactos.contactos',compact('contactos'));
    }
  
}
