<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Art;
use App\Contacto;

class ArtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aseguradoras = Art::all();
        return view('aseguradoras.listacrear',compact('aseguradoras'));
    }

    public function contactos(Art $art)
    {
        $contactos = Contacto::Where('art_id','=',$art->id)->paginate(10);

        return view('contactos.contactos',compact('contactos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $art = new Art;
        $art->nombre = $request->nombre;
        $art->save();
        
        return redirect()->route('arts')->with('status','Aseguradora creada exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Art $art)
    {
        $aseguradora = $art;
        return view('aseguradoras.modificar',compact('aseguradora'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Art $art)
    {
        $art->update([
            'nombre' => $request->nombre
        ]);

        return redirect()->route('arts')->with('status','Aseguradora modificada correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Art $art)
    {
        
        if(count($art->contactos) == 0)
        {
            $art->delete();
            return redirect()->route('arts')->with('status','Aseguradora eliminada.');

        }
        else
        {
            return redirect()->route('arts')->with('no','Aseguradora no puede ser eliminada, contiene clientes asociados.   ');

        }

    }   
}
