<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\Contacto;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {

        View::composer('*', function($view){       
            $view->with('alerts', Contacto::whereDate('fecha_contacto','=', Carbon::now()->toDateString())->count());       
        });



        
    }
}
