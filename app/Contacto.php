<?php

namespace App;

use App\Art;
use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    
    protected $guarded = [];
    
    public function art()
        {
            return $this->belongsTo(Art::class,'art_id');
        }

        
}
