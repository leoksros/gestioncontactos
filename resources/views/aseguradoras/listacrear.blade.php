@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif

            <div class="card">
                <div class="card-header text-center h5">{{ __('Nueva aseguradora') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('storeArt') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="nombre" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}"  required autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                                  

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>

            <div class="card mt-3">
                <div class="card-header text-center h5">{{ __('Aseguradoras') }}</div>

                    <div class="card-body">
                        <ul class="list-group">
    
               
                
                            @forelse ($aseguradoras as $aseguradora)
                            <br>
            
                                 <li class="list-group-item">
                                     <div class="d-flex row">
      
            
                                        <div class="col-sm-10">   
                                        <b>Nombre:</b> <a href="{{route('contactosArt',$aseguradora)}}">{{$aseguradora->nombre}}</a>
                                        <p><b>ID: {{$aseguradora->id}}</b></p>  
                                        </div>

                                         <div class="col-sm-2">
                                             <div class="row">
               
                                                
                                                 <div class="col"> 
                                                    <a href="{{route('editArt',$aseguradora->id)}}">
                                                        <button class="btn btn-warning"> 
                                                            <i class="fas fa-pencil-alt"></i>	
                                                        </button> 
                                                     </a> 
                                                 </div>
            
                                                 <div class="col pt-1 "> 
                                              
                                                    <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('deleteArt',$aseguradora)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger"  type="submit">  
                                                            <i class="fas fa-trash"></i>
                                                        </button>                               
                                                    </form>  
                                                </div>
                                             </div>
                                         </div>
                
                                     </div>   
           
                                 </li>
            
                            @empty
            
                            <div class="row d-flex justify-content-center mt-5 text-center">
                                <div class="col-sm-12">                            
                                    <h3>No existen aseguradoras guardadas.</h3>
                                </div>
                                {{-- <div class="col-sm-12">                            
                                    <a class="text-primary" href="{{route('crearCategoria')}}">Click aquí para registrar categorias!</a>
                                </div> --}}
                                
                            </div>
                            
                            @endforelse
                            
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection