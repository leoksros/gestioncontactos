<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Gestion Contactos') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">


    <!-- Iconos -->

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">


</head>

    
 
    @auth
        <body class="bg-dark">
        

            <div class="wrapper ">
                    <!-- Sidebar  -->
                    <nav id="sidebar">
                        <div class="sidebar-header">
                            <h3>Gestor Contactos</h3>
                            <strong>GS</strong>
                        </div>

                        <ul class="list-unstyled components">

                            <li>
                               
                                <a href="#pageCategorias" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                    <i class="fas fa-copy"></i>
                                    Aseguradoras
                                </a>
                                <ul class="collapse list-unstyled" id="pageCategorias">
                                    <li>
                                        <a href="{{ route('arts') }}">Lista</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('createArt') }}">Nueva</a>
                                    </li>
                                  
                                </ul>
                            </li>


                            <li>
                               
                                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                    <i class="fas fa-copy"></i>
                                    Categorías
                                </a>
                                <ul class="collapse list-unstyled" id="pageSubmenu">
                                    <li>
                                        <a href="{{ route('indexCategorias') }}">Lista</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('indexCategorias') }}">Nueva</a>
                                    </li>
                                  
                                </ul>
                            </li>
                            
                            <li class="">

                                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                                    <i class="fas fa-home"></i>
                                    Contactos
                                </a>

                                <ul class="collapse list-unstyled" id="homeSubmenu">
                                    <li>
                                        <a href="{{ url('/contactos') }}">Lista</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('agregarcontacto') }}">Nuevo</a>
                                    </li>
                                    
                                    
                                </ul>
                            </li>
                            
                            
                            <li>
                                <a href="#">
                                    <i class="fas fa-question"></i>
                                    FAQ
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fas fa-paper-plane"></i>
                                    Contact
                                </a>
                            </li>
                        </ul>

                        <ul class="list-unstyled CTAs ">
                            <li>
                                <a href="" class="download text-center">Download source</a>
                            </li>
                            <li>
                                <a href="" class="article  text-center">Cerrar sesión</a>
                            </li>
                        </ul>
                        
                    </nav>

                    <!-- Page Content  -->
                    <div id="content">

                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <div class="container-fluid">

                                <button type="button" id="sidebarCollapse" class="btn btn-info">
                                    <i class="fas fa-align-left"></i>
                                    <span></span>
                                </button>
                                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <i class="fas fa-align-justify"></i>
                                </button>

                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="nav navbar-nav ml-auto">


                                        @if ($alerts != 0)
                                            <li class="nav-item active">
                                                <a class="nav-link btn btn-primary text-light" href="{{route('tareaPendiente')}}">Pendiente <span class="badge badge-light">{{$alerts}}</span></i></a>
                                            </li>  
                                        @endif
                                        
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('logout') }}">Salir</a>
                                        </li>
                                        

                                    </ul>
                                </div>
                            </div>
                        </nav>

                        
                        @yield('content')
                    </div>
                    
            </div>

            

            <!-- jQuery CDN - Slim version (=without AJAX) -->
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#sidebarCollapse').on('click', function () {
                        $('#sidebar').toggleClass('active');
                    });
                });
            </script>


        </body>
    @endauth

    @guest
        @yield('content')
    @endguest


</html>
