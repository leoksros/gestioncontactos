@extends('layouts.app')


@section('content')

<form method="POST" action="{{ route('searchContacto') }}">
  @csrf
  <div class="input-group mb-3">
    <input type="text" class="form-control" name="busqueda" placeholder="Ingresar búsqueda" aria-label="Recipient's username" aria-describedby="basic-addon2">
    <div class="input-group-append">
      <button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
    </div>
  </div>
</form>


<div class="table-responsive">

  <table class="table table-striped table-dark">
      <thead>
        <tr>
          <th scope="col">Nombre</th>
          <th scope="col">Estado</th>
          <th scope="col">Aseguradora</th>
          <th scope="col">Inicio</th>
          <th scope="col">Email</th>
          <th scope="col">Teléfono</th>
          <th scope="col">Último contacto</th>
          <th scope="col">Fecha contacto</th>        
        </tr>
      </thead>
  
      <tbody>
        <tr>
         
      
          @foreach ($contactos as $contacto) 
  
            
              <tr>               
                  <td>{{ $contacto->nombre }}</td>
                  <td>{{ $contacto->estado }}</td>
                  <td>{{ $contacto->art ? $contacto->art->nombre : 'Sin Art'  }}</td>
                  <td>{{ $contacto->inicio_aseguradora }}</td>
                  <td>{{ $contacto->email }}</td>
                  <td>{{ $contacto->telefono }}</td>
                  <td>{{ $contacto->ultimo_contacto }}</td>
                  <td>{{ $contacto->fecha_contacto }}</td>
                  <td> 
                    
                      <div class="row">
    
                        <div class="col">
                            <a href="{{route('editarcontacto',$contacto)}}" class="btn btn-warning"><i class="fas fa-pencil-alt "></i></a> 
                        </div>
                        
                        
                        <div class="col pt-1 ">
                        <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('deleteContacto',$contacto)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger"> 
                                <i class="fas fa-trash"></i>
                            </button>                             
                          </form>
                        </div>
    
                      </div>
                  </td>
              </tr>                
          @endforeach
              
        
      </tbody>
  
    </table>
</div>

  {{$contactos->links()}}

@endsection