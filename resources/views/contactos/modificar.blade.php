@extends('layouts.app')



@section('content')
    

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('actualizarcontacto',$contacto) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre',$contacto["nombre"]) }}"  required autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="estado" class="col-md-4 col-form-label text-md-right">{{ __('Estado') }}</label>

                            <div class="col-md-6">
                                <input id="estado" type="text" class="form-control @error('estado') is-invalid @enderror" name="estado" value="{{ old('estado',$contacto["estado"]) }}"  autocomplete="estado" autofocus>

                                @error('estado')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="art_id" class="col-md-4 col-form-label text-md-right">{{ __('ART') }}</label>

                            <div class="col-md-6">                                

                                <select name="art_id" class="form-control">
                                    @foreach($arts as $art)
                                        @if($art->nombre == $contacto->art->nombre)
                                            <option selected value="{{ $art->id }}"> {{$art->nombre}}</option> 
                                        @else
                                            <option value="{{ $art->id }}"> {{$art->nombre}}</option> 
                                        @endif
                                    @endforeach                    
                                </select>

                                @error('art_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inicio_aseguradora" class="col-md-4 col-form-label text-md-right">{{ __('Inicio') }}</label>

                            <div class="col-md-6">
                                <input id="inicio_aseguradora" type="date" class="form-control @error('inicio_aseguradora') is-invalid @enderror" name="inicio_aseguradora" value="{{ old('inicio_aseguradora',$contacto["inicio_aseguradora"]) }}"  autocomplete="inicio_aseguradora" autofocus>

                                @error('inicio_aseguradora')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email',$contacto["email"]) }}"  autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telefono" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono') }}</label>

                            <div class="col-md-6">
                                <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono"  autocomplete="telefono" value="{{ old('telefono',$contacto["telefono"]) }}">

                                @error('telefono')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_contacto" class="col-md-4 col-form-label text-md-right">{{ __('Fecha contacto') }}</label>

                            <div class="col-md-6">
                                <input id="fecha_contacto" type="date" class="form-control @error('fecha_contacto') is-invalid @enderror" name="fecha_contacto"  autocomplete="fecha_contacto" value="{{ old('fecha_contacto',$contacto["fecha_contacto"]) }}">

                                @error('fecha_contacto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ultimo_contacto" class="col-md-4 col-form-label text-md-right">{{ __('Último contacto') }}</label>

                            <div class="col-md-6">
                                <input id="ultimo_contacto" type="date" class="form-control @error('ultimo_contacto') is-invalid @enderror" name="ultimo_contacto"  autocomplete="ultimo_contacto" value="{{ old('ultimo_contacto',$contacto["ultimo_contacto"]) }}">

                                @error('ultimo_contacto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection