@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center h5">{{ __('Nuevo contacto') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registrarcontacto') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="estado" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Estado') }}</label>

                            <div class="col-md-6">
                                <input id="estado" type="text" class="form-control @error('estado') is-invalid @enderror" name="estado" value="{{ old('estado') }}"  autocomplete="estado" autofocus>

                                @error('estado')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="categoria_id" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Categoría') }}</label>

                            <div class="col-md-6">                                

                                <select name="categoria_id" class="form-control">
                                    @foreach($categorias as $categoria)
                                        @if($categoria->nombre == 'Ninguna')
                                            <option selected value="{{ $categoria->id }}"> {{$categoria->nombre}}</option> 
                                        @else
                                            <option value="{{ $categoria->id }}"> {{$categoria->nombre}}</option> 
                                        @endif
                                    @endforeach                    
                                </select>

                                @error('categoria_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        

                        <div class="form-group row">
                            <label for="art_id" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Aseguradora') }}</label>

                            <div class="col-md-6">                                

                                <select name="art_id" class="form-control">
                                    @foreach($arts as $art)
                                        @if($art->nombre == 'Ninguna')
                                            <option selected value="{{ $art->id }}"> {{$art->nombre}}</option> 
                                        @else
                                            <option value="{{ $art->id }}"> {{$art->nombre}}</option> 
                                        @endif
                                    @endforeach                    
                                </select>

                                @error('art_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inicio_aseguradora" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Inicio') }}</label>

                            <div class="col-md-6">
                                <input id="inicio_aseguradora" type="date" class="form-control @error('inicio_aseguradora') is-invalid @enderror" name="inicio_aseguradora" value="{{ old('inicio_aseguradora') }}"  autocomplete="inicio_aseguradora" autofocus>

                                @error('inicio_aseguradora')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telefono" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Teléfono') }}</label>

                            <div class="col-md-6">
                                <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono"  autocomplete="telefono">

                                @error('telefono')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_contacto" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Fecha contacto') }}</label>

                            <div class="col-md-6">
                                <input id="fecha_contacto" type="date" class="form-control @error('fecha_contacto') is-invalid @enderror" name="fecha_contacto"  autocomplete="fecha_contacto">

                                @error('fecha_contacto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ultimo_contacto" class="font-weight-bold col-md-4 col-form-label text-md-right">{{ __('Último contacto') }}</label>

                            <div class="col-md-6">
                                <input id="ultimo_contacto" type="date" class="form-control @error('ultimo_contacto') is-invalid @enderror" name="ultimo_contacto"  autocomplete="ultimo_contacto">

                                @error('ultimo_contacto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection