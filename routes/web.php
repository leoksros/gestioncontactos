<?php

use Illuminate\Support\Facades\Route;


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

/* Contactos */

Route::get('contactos','ContactosController@index')->name('contactos')->middleware('auth');
Route::get('contactos/pendientes','ContactosController@pendientes')->name('tareaPendiente')->middleware('auth');
Route::get('contactos/agregar','ContactosController@create')->name('agregarcontacto')->middleware('auth');
Route::post('contactos/registrar','ContactosController@store')->name('registrarcontacto')->middleware('auth');
Route::get('contactos/{contacto}','ContactosController@edit')->name('editarcontacto')->middleware('auth');
Route::put('contactos/modificar/{contacto}','ContactosController@update')->name('actualizarcontacto')->middleware('auth');
Route::post('contactos/importar','ContactosController@import')->name('importar')->middleware('auth');
Route::post('contactos/buscar','ContactosController@search')->name('searchContacto')->middleware('auth');
Route::delete('contactos/{contacto}/borrar','ContactosController@destroy')->name('deleteContacto')->middleware('auth');


/* Categorías */

Route::get('categorias','CategoriasController@index')->name('indexCategorias')->middleware('auth');
Route::get('categorias/{categoria}/contactos','CategoriasController@contactos')->name('contactosCategoria')->middleware('auth');
Route::get('categorias/{categoria}/mostrar','CategoriasController@index')->name('showCategoria')->middleware('auth');
Route::post('categorias/nueva','CategoriasController@store')->name('storeCategoria')->middleware('auth');
Route::get('categorias/{categoria}/editar','CategoriasController@edit')->name('editCategoria')->middleware('auth');
Route::put('categorias/{categoria}/update','CategoriasController@update')->name('updateCategoria')->middleware('auth');
Route::delete('categorias/categoria/{categoria}/delete','CategoriasController@destroy')->name('deleteCategoria')->middleware('auth');

/* Art */

Route::get('aseguradoras/art','ArtController@index')->name('arts')->middleware('auth');
Route::get('aseguradoras/art/{art}/contactos','ArtController@contactos')->name('contactosArt')->middleware('auth');
Route::get('aseguradoras/art/nueva','ArtController@index')->name('createArt')->middleware('auth');
Route::post('aseguradoras/art/guardar','ArtController@store')->name('storeArt')->middleware('auth');
Route::get('aseguradoras/art/{art}/editar','ArtController@edit')->name('editArt')->middleware('auth');
Route::put('aseguradoras/art/{art}/update','ArtController@update')->name('updateArt')->middleware('auth');
Route::delete('aseguradoras/art/{art}/delete','ArtController@destroy')->name('deleteArt')->middleware('auth');












